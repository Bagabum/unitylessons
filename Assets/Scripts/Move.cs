﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

    [SerializeField]
    private int speed = 5;
    [SerializeField]
    private int jump = 15;

    // Use this for initialization
    void Start () {
        //transform.position = new Vector3(0, 0, 0);
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(new Vector3(-speed * Time.deltaTime, 0, 0));
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(new Vector3(0, 0, speed * Time.deltaTime));
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(new Vector3(0, 0, -speed * Time.deltaTime));
        }
        if (Input.GetKey(KeyCode.Space))
        {
            Rigidbody rig = GetComponent<Rigidbody>();
            rig.AddForce(new Vector3(0, 15, 0), ForceMode.Impulse);
        }
    }
}
